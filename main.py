import os
from cv2 import cv2
import numpy as np
from numpy import linalg as LA

def extractingColorChannel(channel, cell_size, w, h):
    channel_his = []
    for cx in range (w//cell_size):
        for cy in range (h//cell_size):
            channel_xy = channel[cy*cell_size:cy*cell_size+cell_size, cx*cell_size:cx*cell_size+cell_size]
            channel_his_xy = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] 
            for i in range (cell_size):
                for j in range (cell_size):
                    if (channel_xy[i][j] >= 0) and (channel_xy[i][j] < 25): channel_his_xy[0] += 1
                    elif (channel_xy[i][j] >= 25) and (channel_xy[i][j] < 50): channel_his_xy[1] += 1
                    elif (channel_xy[i][j] >= 50) and (channel_xy[i][j] < 50): channel_his_xy[2] += 1
                    elif (channel_xy[i][j] >= 75) and (channel_xy[i][j] < 50): channel_his_xy[3] += 1
                    elif (channel_xy[i][j] >= 100) and (channel_xy[i][j] < 50): channel_his_xy[4] += 1
                    elif (channel_xy[i][j] >= 125) and (channel_xy[i][j] < 50): channel_his_xy[5] += 1
                    elif (channel_xy[i][j] >= 150) and (channel_xy[i][j] < 50): channel_his_xy[6] += 1
                    elif (channel_xy[i][j] >= 175) and (channel_xy[i][j] < 50): channel_his_xy[7] += 1
                    elif (channel_xy[i][j] >= 200) and (channel_xy[i][j] < 50): channel_his_xy[8] += 1
                    elif (channel_xy[i][j] >= 225) and (channel_xy[i][j] <= 255): channel_his_xy[9] += 1
                pass
            pass
            channel_his = channel_his + channel_his_xy
        pass
    pass
    return channel_his

def extractingColor(img_path, cell_size=8):
    img = cv2.imread(img_path)
    img = cv2.resize(src=img, dsize=(64, 128))
    h, w, _ = img.shape
    b, g, r = cv2.split(img)
    
    hb = extractingColorChannel(b, cell_size, w, h)
    hg = extractingColorChannel(g, cell_size, w, h)
    hr = extractingColorChannel(r, cell_size, w, h)

    histogramColor = hb + hg + hr
    histogramColor = np.array(histogramColor)
    histogramColor = histogramColor/LA.norm(histogramColor, 2)
    np.seterr(divide='ignore', invalid='ignore')

    return histogramColor

def extractingShape(img_path, block_size=2, cell_size=8, bins=9):
    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(src=img, dsize=(64, 128))
    h, w = img.shape 
    
    matrix_x = np.array([[-1, 1]])
    matrix_y = np.array([[-1], [1]])
    dx = cv2.filter2D(img, cv2.CV_32F, matrix_x)
    dy = cv2.filter2D(img, cv2.CV_32F, matrix_y)
    
    cuong_do = np.sqrt(np.square(dx) + np.square(dy))
    goc = np.arctan(np.divide(dy, dx+0.00001))
    goc = np.degrees(goc)
    goc += 90
    
    num_cell_x = w // cell_size 
    num_cell_y = h // cell_size 
    
    hist = np.zeros([num_cell_y, num_cell_x, bins])
    for cx in range(num_cell_x):
        for cy in range(num_cell_y):
            goc_xy = goc[cy*cell_size:cy*cell_size+cell_size, cx*cell_size:cx*cell_size+cell_size]
            cuong_do_xy = cuong_do[cy*cell_size:cy*cell_size+cell_size, cx*cell_size:cx*cell_size+cell_size]
            h, _ = np.histogram(goc_xy, bins=bins, range=(0, 180), weights=cuong_do_xy) 
            hist[cy, cx, :] = h
        pass
    pass
    
    redundant_cell = block_size-1
    f = np.zeros([num_cell_y-redundant_cell, num_cell_x-redundant_cell, block_size*block_size*bins])
    for bx in range(num_cell_x-redundant_cell):
        for by in range(num_cell_y-redundant_cell):
            by_from = by
            by_to = by+block_size
            bx_from = bx
            bx_to = bx+block_size
            v = hist[by_from:by_to, bx_from:bx_to, :].flatten() 
            f[by, bx, :] = v / LA.norm(v, 2)
            # avoid NaN:
            if np.isnan(f[by, bx, :]).any(): 
                f[by, bx, :] = v
    
    return f.flatten()
    
def getCharacterFeature(dirPath, label):
    characterFeatures = []
    c = os.listdir(dirPath)
    for file in c:
        color = extractingColor(dirPath + '/' + file)
        shape = extractingShape(dirPath + '/' + file)
        v = np.concatenate((color, shape, [label]), axis=0)
        print(file, end=' ')
        characterFeatures.append(v)
    characterFeatures = np.array(characterFeatures)
    return characterFeatures

def getAllCharacterFeature():
    doraemon = getCharacterFeature('data/doraemon', 0)
    jerry = getCharacterFeature('data/jerry', 1)
    nobita = getCharacterFeature('data/nobita', 2)
    tom = getCharacterFeature('data/tom', 3)

    features = np.concatenate((doraemon, jerry, nobita, tom), axis=0)
    
    return features

def distanceCounting(v, features):
    minD = 9999999999
    label = ''
    nearestFeature = []
    for f in features:
        d = 0
        for i in range (len(f)-1):
            d += np.square(v[i]-f[i])
        if(d < minD):
            minD = d
            label = f[len(f)-1]
            nearestFeature = f
            
    return np.sqrt(minD), label, nearestFeature

def identify(features, img_path):
    img = cv2.imread(img_path)
    color = extractingColor(img_path)
    shape = extractingShape(img_path)
    v = np.concatenate((color, shape), axis=0)

    nearestDistance, label, nearestFeature = distanceCounting(v, features)
    name = ''
    if (label == 0): name = 'doraemon'
    elif (label == 1): name = 'jerry'
    elif (label == 2): name = 'nobita'
    elif (label == 3): name = 'tom'
    print(nearestDistance)
    print(v)
    print(nearestFeature)
    
    font = cv2.FONT_HERSHEY_SIMPLEX
    cv2.putText(img, name, (10,50), font, 2, (255, 255, 255), 2, cv2.LINE_AA)

    cv2.imshow('input image', img)
    cv2.waitKey(0)

features = np.load('features.npy')
identify(features, 'input.jpg')